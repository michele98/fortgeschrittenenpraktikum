import numpy as np
import copy
import uncertainties
import string
import scipy.odr.odrpack as odrpack

import matplotlib.pyplot as plt
from jupyterthemes import jtplot

class ShorthandFormatter(string.Formatter):

    def format_field(self, value, format_spec):
        if isinstance(value, uncertainties.UFloat):
            return value.format(format_spec+'S')  # Shorthand option added
        # Special formatting for other types can be added here (floats, etc.)
        else:
            # Usual formatting:
            return super(ShorthandFormatter, self).format_field(
                value, format_spec)

def fit_odr(f,x,y, xs, ys, beta_0):
    #ODR fitting
    #print ('\n')
    model = odrpack.Model(f)
    data = odrpack.RealData(x, y, sx=xs, sy=ys)
    myodr = odrpack.ODR(data, model, beta0=beta_0)
    output = myodr.run()
    #output.pprint()
    return output

def relu(arr):
    ret = np.zeros(len(arr))
    for i in range(len(arr)):
        if arr[i] > 0:
            ret[i] = arr[i]
    return ret

def weighted_average(values, errors):
    weights = np.array([1/error**2 for error in errors])
    vals = np.array(values)
    return sum(vals*weights)/sum(weights), np.sqrt(1/sum(weights))

def chisquare(obs, expected, alpha = 1):
    if type(alpha) == int: alpha = np.zeros(len(obs))+1
    return sum(((np.array(obs)-np.array(expected))/np.array(alpha))**2)

def red_chisquare(obs, expected, alpha, ddof): #ddof: degrees of freedom = anzahl Datenpunkte - freie Parameter
    return chisquare(obs, expected, alpha)/ddof

#running average filter
def running_average(arr, L = 1, add_edges = False):
    #returns input immediately if the filter length is 1
    if L == 1 or L == 0:
        return arr

    #for uneven filter length, take average of values left and right around each point
    if L%2 == 1:
        l = int((L-1)/2)
        if add_edges:
            return np.array([0 for i in range(l)] + [sum(arr[i-l:i+l+1])/L for i in range(l, len(arr)-l)] + [0 for i in range(l)])
        else:
            return np.array([sum(arr[i-l:i+l+1])/L for i in range(l, len(arr)-l)])

    #for even filter length, take average value of points around a point between values
    else:
        l = int(L/2)
        if add_edges:
            return np.array([0 for i in range(l)] + [sum(arr[i-l:i+l])/L for i in range(l, len(arr)-l+1)] + [0 for i in range(l-1)])
        else:
            return np.array([sum(arr[i-l:i+l])/L for i in range(l, len(arr)-l+1)])

#plot formatting for jupyter notebooks
def dp(height = 1080, aspect = 4/3, dpi = 300):
    jtplot.style(theme = 'monokai', grid = False, ticks = True)
    plot_resize(height, aspect, dpi)

def lp(height = 1080, aspect = 4/3, dpi = 300):
    jtplot.style(theme = 'default', grid = False, ticks = True)
    plot_resize(height, aspect, dpi)
    
def rp():
    jtplot.reset()
    plot_resize()

def plot_resize(height = 1080, aspect = 4/3, dpi = 300):
    plt.rcParams['figure.figsize'] = (height*aspect/dpi, height/dpi)
    plt.rcParams['figure.dpi'] = dpi
    plt.rcParams.update({'figure.subplot.top': 0.98, 'figure.subplot.bottom': 0.2, 'figure.subplot.left': 0.18, 'figure.subplot.right': 0.98})