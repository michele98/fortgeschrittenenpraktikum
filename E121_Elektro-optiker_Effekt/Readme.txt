For the EOM experiment, answer the questions on the script and submit your answers, individually for each member, together with a collaborative theory section for the report by Monday following the scheduled date.
The answers can come into any format (not necessarily latex), as far as they are readable.
For the second part, I have provided you some typical data traces to analyse. Please make the analysis individually and submit your final reports within two weeks of the scheduled date.
The reports should differ for the data analysis part, done independently by each member, all other parts can be identical.
The part on the realization of the experiment will be just a placeholder, to be integrated once the actual experiment will be realized.

Data were taken by building the setup described in the script (Abbildung 2). 
Most of the experimental challenge lies in optimizing the alignment and reaching a high contrast for the interference signal. 
This part of the experiment will hopefully take place in the lab later on. 

In these folders, you find the measurements of the interference fringes as recorded on an oscilloscope.
There are two measurement series, one for the bandwidth measurement and one for the optical switch (see script).
In each measurement folder you will find two channel files as well as a picture that shows you how the screen looked like. 
Channel 2 is the signal from the photodiode and channel 3 is the voltage ramp applied to the EOM.
Please note that the voltage ramp goes through an amplifier such that, on the EOM, the amplitude is 1000 times larger than what recorded on the scope.
The channel files contain information on what time and voltage scale was used for the measurement.
Extract the data and fit it appropriately to complete the tasks as described in the script.

For further information, please write me at Manuele.Landini@uibk.ac.at